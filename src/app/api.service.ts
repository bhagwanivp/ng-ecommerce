import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Companies } from './companies';
import { Products } from './products';
import { Orders } from './orders';
import { LocalstoreService } from './localstore.service';

const {} = LocalstoreService;
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};
const apiUrl = '/api/';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(
    private http: HttpClient,
    private localStore: LocalstoreService
  ) {}

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getCompanies(): Observable<Companies[]> {
    return this.http.get<Companies[]>(`${apiUrl}`).pipe(
      tap((companies) => {
        this.localStore.setLocalData('companies', companies);
        console.log('fetched Companies', companies);
      }),
      catchError(this.handleError('getCompanies', []))
    );
  }

  addCompany(company: Companies): Observable<Companies> {
    return this.http.post<Companies>(apiUrl, company, httpOptions).pipe(
      tap((c: Companies) => {
        const allCompanies = this.localStore.getLocalData('companies') || [];
        allCompanies.push(c);
        this.localStore.setLocalData('companies', allCompanies);
        console.log(`added Companies w/ id=${c.id}`);
      }),
      catchError(this.handleError<Companies>('addCompanies'))
    );
  }

  updateCompany(id: string, company: Companies): Observable<any> {
    const url = `${apiUrl}/${id}`;
    return this.http.put(url, company, httpOptions).pipe(
      tap((_) => {
        const allCompanies = this.localStore.getLocalData('companies') || [];
        const i = allCompanies.findIndex((comp: Companies) => comp.id === id);
        if (i !== -1) {
          allCompanies.splice(i, 1, company);
        }
        this.localStore.setLocalData('companies', allCompanies);
        console.log(`updated Company id=${id}`);
      }),
      catchError(this.handleError<any>('updateCompany'))
    );
  }

  deleteCompany(id: string): Observable<Companies> {
    const url = `${apiUrl}/${id}`;
    return this.http.delete<Companies>(url, httpOptions).pipe(
      tap((_) => {
        const allCompanies = this.localStore.getLocalData('companies') || [];
        const i = allCompanies.findIndex((comp: Companies) => comp.id === id);
        if (i !== -1) {
          allCompanies.splice(i, 1);
        }
        this.localStore.setLocalData('companies', allCompanies);
        console.log(`deleted Company id=${id}`);
      }),
      catchError(this.handleError<Companies>('deleteCompany'))
    );
  }

  getProducts(): Observable<Products[]> {
    return this.http.get<Products[]>(`${apiUrl}`).pipe(
      tap((products) => {
        this.localStore.setLocalData('companies', products);
        console.log('fetched Products', products);
      }),
      catchError(this.handleError('getProducts', []))
    );
  }

  addProduct(product: Products): Observable<Products> {
    return this.http.post<Products>(apiUrl, product, httpOptions).pipe(
      tap((c: Products) => {
        const allProducts = this.localStore.getLocalData('products') || [];
        allProducts.push(c);
        this.localStore.setLocalData('products', allProducts);
        console.log(`added Product w/ id=${c.id}`);
      }),
      catchError(this.handleError<Products>('addProduct'))
    );
  }

  updateProduct(id: string, product: Products): Observable<any> {
    const url = `${apiUrl}/${id}`;
    return this.http.put(url, product, httpOptions).pipe(
      tap((_) => {
        const allProducts = this.localStore.getLocalData('products') || [];
        const i = allProducts.findIndex((prod: Products) => prod.id === id);
        if (i !== -1) {
          allProducts.splice(i, 1, product);
        }
        this.localStore.setLocalData('products', allProducts);
        console.log(`updated product id=${id}`);
      }),
      catchError(this.handleError<any>('updateProduct'))
    );
  }

  deleteProduct(id: string): Observable<Products> {
    const url = `${apiUrl}/${id}`;
    return this.http.delete<Products>(url, httpOptions).pipe(
      tap((_) => {
        const allProducts = this.localStore.getLocalData('products') || [];
        const i = allProducts.findIndex((prod: Products) => prod.id === id);
        if (i !== -1) {
          allProducts.splice(i, 1);
        }
        this.localStore.setLocalData('products', allProducts);
        console.log(`deleted product id=${id}`);
      }),
      catchError(this.handleError<Products>('deleteProduct'))
    );
  }

  getOrders(): Observable<Orders[]> {
    return this.http.get<Orders[]>(`${apiUrl}`).pipe(
      tap((orders) => {
        this.localStore.setLocalData('orders', orders);
        console.log('fetched Orders', orders);
      }),
      catchError(this.handleError('getOrders', []))
    );
  }

  addOrder(order: Orders): Observable<Orders> {
    return this.http.post<Orders>(apiUrl, order, httpOptions).pipe(
      tap((c: Orders) => {
        const allOrders = this.localStore.getLocalData('orders') || [];
        allOrders.push(c);
        this.localStore.setLocalData('orders', allOrders);
        console.log(`added Orders w/ id=${c.id}`);
      }),
      catchError(this.handleError<Orders>('addOrder'))
    );
  }

  updateOrder(id: string, order: Orders): Observable<any> {
    const url = `${apiUrl}/${id}`;
    return this.http.put(url, order, httpOptions).pipe(
      tap((_) => {
        console.log(`updated order id=${id}`);
      }),
      catchError(this.handleError<any>('updateOrder'))
    );
  }

  deleteOrder(id: string): Observable<Orders> {
    const url = `${apiUrl}/${id}`;
    return this.http.delete<Orders>(url, httpOptions).pipe(
      tap((_) => console.log(`deleted Order id=${id}`)),
      catchError(this.handleError<Orders>('deleteOrder'))
    );
  }
}
