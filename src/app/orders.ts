export class Orders {
  id: string;
  orderNumber: string;
  company: string;
  product: string;
  rate: string;
  quantity: number;
  totalPrice: string;
}
