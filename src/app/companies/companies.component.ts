import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Companies } from '../companies';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss'],
})
export class CompaniesComponent implements OnInit {
  constructor(private api: ApiService) {}
  displayedColumns: string[] = ['id', 'name', 'gst'];
  data: Companies[] = [];
  isLoadingResults = true;

  ngOnInit(): void {
    this.api.getCompanies().subscribe(
      (res: any) => {
        this.data = res;
        console.log(this.data);
        this.isLoadingResults = false;
      },
      (err) => {
        console.log(err);
        this.isLoadingResults = false;
      }
    );
  }
}
