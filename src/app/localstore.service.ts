import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LocalstoreService {
  constructor() {}

  getLocalData(itemName: string) {
    return JSON.parse(localStorage.getItem(itemName) || '{}');
  }
  setLocalData(itemName: string, itemValue: any) {
    return localStorage.setItem(itemName, JSON.stringify(itemValue));
  }
  removeLocalData(itemName: string) {
    return localStorage.setItem(itemName, null);
  }
}
