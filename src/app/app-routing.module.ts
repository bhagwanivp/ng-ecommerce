import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompaniesComponent } from './companies/companies.component';
import { OrdersComponent } from './orders/orders.component';
import { ProductsComponent } from './products/products.component';

const routes: Routes = [
  {
    path: 'companies',
    component: CompaniesComponent,
    data: { title: 'Manage Companies' },
  },
  {
    path: 'products',
    component: ProductsComponent,
    data: { title: 'Manage Products' },
  },
  {
    path: 'orders',
    component: OrdersComponent,
    data: { title: 'Manage Orders' },
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
